from J6305edit import Spectrometer  #the J6305 file was edited slightly in the scan function to allow part of this code to work, hence why i am importing from a file named J6305edit
import time
spec = Spectrometer()
import sys


calibrate = input("Please enter your calibration sample, type 'g'to proceed or 'n' to cancel: ")
if calibrate.lower() == "g":
    print("Spec has been calibrated, remove this sample now ")
    spec.calibrate()    #Calibrating Spectrophotometer

else:
    print("Goodbye")                        #if the user enters anythin other than 'g' then the program will stop running without crashing
    sys.exit()

start = input("Please enter your first sample, type 'yes' to proceed or 'n' to cancel: ")            #asking the user to enter the starting, end, and interval wavelength at which it will scan
if start.lower() == 'yes':
        start1 = int(input("State the wavelength at which you would like to start: "))
        start = start1
        end1 = int(input("Now enter an end wavelength: "))
        end = end1
        interval1 = int(input("Now enter the interval in which wavelength will increase: "))
        interval = interval1

        
        data=[]
        spec.set_wavelength(start)
        spec.pause(1)          #pause allows the spec to keep up 
        for wl in range(start1, end1+interval1, interval1):
            spec.set_wavelength(wl)                           #scans at the desired wavelength then appends this data
            spec.pause(0.5)
            while spec.absorbance()[0] is None:                #This part of code was adopted from the J6305edit program
                pass
            data.append(spec.absorbance())
            print("Scanning.........")                #lets user know each time the spec scans at a certain wavelength



        #writing data to a file for sample 1 in a readable manner to a text file named 'results1'
            
            ofh=open('results1','w')
            scans=spec.scan(start1, end1+interval1, interval1)
            ofh.write('Abs\tWavelength\n')                        #This part of code was adopted from the J6305edit program
            for s in scans:
                ofh.write('\t'.join([str(t) for t in s]) + '\n')
            ofh.close()
            print("Your data for wavelength %s nm to %s nm increasing in intervals of %s nm has been saved to a file named results1 " %(start1,end1,interval1))  #simply letting the user know what data has been saved to teh text file and the name of it making it more user friendly
else:
    print("Goodbye")        #if the user enters anythin other than 'yes' then the program will stop running without crashing
    sys.exit()

#------------------------------------------------------------------------------------- the following code is a repeat of what has been talked about above, allowing up to 4 different samples 

#sample 2

calibrate = input("Please enter your calibration sample for your second sample, type 'g'to proceed or 'n' to cancel: ")
if calibrate.lower() == "g":
    print("Spec has been calibrated, remove this sample now")
    spec.calibrate()


start2 = input("Please enter your second sample, type 'yes2' to proceed or 'n' to cancel: ")
if start2.lower() == 'yes2':
        start2 = int(input("State the wavelength at which you would like to start: "))
        start = start2
        end2 = int(input("Now enter an end wavelength: "))
        end = end2
        interval2 = int(input("Now enter the interval in which wavelength will increase: "))
        interval = interval2


        data=[]
        spec.set_wavelength(start)
        spec.pause(0.5)
        for wl in range(start2, end2+interval2, interval2):
            spec.set_wavelength(wl)
            spec.pause(0.5)
            while spec.absorbance()[0] is None:
                pass
            data.append(spec.absorbance())
            
            print("Scanning......")

                    #writing data to a file for sample 2 in a file called results2 
            ofh=open('results2','w')
            scans=spec.scan(start2, end2+interval2, interval2)
            ofh.write('Abs\tWavelength\n')
            for s in scans:
                ofh.write('\t'.join([str(t) for t in s]) + '\n')
            ofh.close()
            print("Your data for wavelength %s nm to %s nm increasing in intervals of %s nm has been saved to a file named results2 " %(start2,end2,interval2))
else:
    print("Goodbye")
    sys.exit()
#____________________________________________________________________________________________
#sample 3
    
calibrate = input("Please enter your calibration sample, type 'g'to proceed or 'n' to cancel: ")
if calibrate.lower() == "g":
    print("Spec has been calibrated, remove this sample now ")
    spec.calibrate()    #Calibrating Spectrophotometer

else:
    print("Goodbye")
    sys.exit()

start3 = input("Please enter your 3rd sample, type 'yes3' to proceed or 'n' to cancel: ")
if start3.lower() == 'yes3':
        start3 = int(input("State the wavelength at which you would like to start: "))
        start = start3
        end3 = int(input("Now enter an end wavelength: "))
        end = end3
        interval3 = int(input("Now enter the interval in which wavelength will increase: "))
        interval = interval3

        
        data=[]
        spec.set_wavelength(start)
        spec.pause(1)
        for wl in range(start3, end3+interval3, interval3):
            spec.set_wavelength(wl)
            spec.pause(0.5)
            while spec.absorbance()[0] is None:
                pass
            data.append(spec.absorbance())
            print("Scanning.........")



        #writing data to a file for sample 1
            
            ofh=open('results3','w')
            scans=spec.scan(start3, end3+interval3, interval3)
            ofh.write('Abs\tWavelength\n')
            for s in scans:
                ofh.write('\t'.join([str(t) for t in s]) + '\n')
            ofh.close()
            print("Your data for wavelength %s nm to %s nm increasing in intervals of %s nm has been saved to a file named results3 " %(start3,end3,interval3))
else:
    print("Goodbye")
    sys.exit()

#_________________---------------------------------------____________________________________________________________________--
#sample 4

    
calibrate = input("Please enter your calibration sample, type 'g'to proceed or 'n' to cancel: ")
if calibrate.lower() == "g":
    print("Spec has been calibrated, remove this sample now ")
    spec.calibrate()    #Calibrating Spectrophotometer

else:
    print("Goodbye")
    sys.exit()

start = input("Please enter your fourth sample, type 'yes4' to proceed or 'n' to cancel: ")
if start.lower() == 'yes4':
        start4 = int(input("State the wavelength at which you would like to start: "))
        start = start4
        end4 = int(input("Now enter an end wavelength: "))
        end = end4
        interval4 = int(input("Now enter the interval in which wavelength will increase: "))
        interval = interval4

        
        data=[]
        spec.set_wavelength(start)
        spec.pause(1)
        for wl in range(start4, end4+interval4, interval4):
            spec.set_wavelength(wl)
            spec.pause(0.5)
            while spec.absorbance()[0] is None:
                pass
            data.append(spec.absorbance())
            print("Scanning.........")



        #writing data to a file for sample 4
            
            ofh=open('results1','w')
            scans=spec.scan(start4, end4+interval4, interval4)
            ofh.write('Abs\tWavelength\n')
            for s in scans:
                ofh.write('\t'.join([str(t) for t in s]) + '\n')
            ofh.close()
            print("Your data for wavelength %s nm to %s nm increasing in intervals of %s nm has been saved to a file named results4 " %(start4,end4,interval4))
else:
    print("Goodbye")
    sys.exit()

#-----------------------------------------------------------------------------------------




